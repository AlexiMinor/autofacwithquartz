﻿using System.Web.Http;
using System.Web.Http.Dependencies;
using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;
using AutofacWithQuartzNet.DependencyResolver;
using Quartz;
using Quartz.Impl;
using WebGrease.Configuration;

namespace AutofacWithQuartzNet.Jobs
{
    public class DbWriterScheduler
    {
        public async void Start()
        {
            IScheduler sched = AutofacConfig.Container.Resolve<IScheduler>();
            sched.JobFactory = new AutofacJobFactory(AutofacConfig.Container);
            await sched.Start();

            var job = JobBuilder.Create<DBWriter>()
                .WithIdentity("Job")
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity("JobTrigger")
                .WithSimpleSchedule(x => x
                    .RepeatForever()
                    .WithIntervalInSeconds(5)
                )
                .StartNow()
                .Build();

            await sched.ScheduleJob(job, trigger);
        }
    }
}