﻿using Core.Entities;

namespace DAL
{
    public class ArticlesRepository : Repository<Article>
    {
        public ArticlesRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}