﻿using System.Threading.Tasks;

namespace Core.Interfaces.Services
{
    public interface IService
    {
        Task DoAsync();
    }
}