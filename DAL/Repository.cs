﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;

namespace DAL
{
    public class Repository<T> : IRepository<T> where T : class, IBaseEntity
    {
        protected readonly ApplicationDbContext Db;
        protected readonly DbSet<T> DbSet;

        public Repository(ApplicationDbContext context)
        {
            Db = context;
            DbSet = Db.Set<T>();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }

        public async Task AddAsync(T obj)
        {
            DbSet.Add(obj);
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await DbSet.AsNoTracking().FirstOrDefaultAsync(arg => arg.Id.Equals(id));
        }

        public async Task<IQueryable<T>> GetAllAsync()
        {
            return DbSet;
        }

        public async Task<IQueryable<T>> FindByAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            var result = DbSet.Where(predicate);
            if (includes.Any())
            {
                result = includes.Aggregate(result, (current, include) => current.Include(include));
            }

            return result;
        }

        public async Task UpdateAsync(T obj)
        {
            DbSet.AddOrUpdate(obj);
        }

        public async Task RemoveAsync(int id)
        {
            DbSet.Remove(await GetByIdAsync(id));
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Db.SaveChangesAsync();
        }
    }
}
