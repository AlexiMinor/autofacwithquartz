﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class, IBaseEntity
    {
        Task AddAsync(TEntity obj);
        Task<TEntity> GetByIdAsync(int id);
        Task<IQueryable<TEntity>> GetAllAsync();
        Task<IQueryable<TEntity>> FindByAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);
        Task UpdateAsync(TEntity obj);
        Task RemoveAsync(int id);
        Task<int> SaveChangesAsync();
    }
}
