﻿namespace Core.Entities
{
    public class Article : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FullText { get; set; }
    }
}
