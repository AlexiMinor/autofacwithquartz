﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using AutofacWithQuartzNet.Jobs;
using Core.Entities;
using Core.Interfaces;
using Core.Interfaces.Services;
using DAL;
using Quartz;
using Quartz.Impl;

namespace AutofacWithQuartzNet.DependencyResolver
{
    public class AutofacConfig
    {
        public static IContainer Container;

        public static void ConfigureContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterWebApiFilterProvider(config);

            builder.RegisterWebApiModelBinderProvider();

            builder.RegisterType<ArticlesRepository>()
                .As<IRepository<Article>>().WithParameter("context", new ApplicationDbContext());
            builder.RegisterType<TestService.TestService>()
                .As<IService>();

            builder.Register(x => new StdSchedulerFactory().GetScheduler().Result).As<IScheduler>();

            //for all possible jobs
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(x => typeof(IJob).IsAssignableFrom(x));

            Container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(Container);


            //Schedule
         

        }
    }
}