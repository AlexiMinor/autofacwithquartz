﻿using System.Threading.Tasks;
using Core.Interfaces.Services;
using Quartz;

namespace AutofacWithQuartzNet.Jobs
{
    public class DBWriter : IDbWriterJob
    {
        private readonly IService _service;

        public DBWriter(IService service)
        {
            _service = service;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            await _service.DoAsync();
        }
    }
}