﻿using System;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Interfaces.Services;

namespace TestService
{
    public class TestService : IService
    {
        private readonly IRepository<Article> _repository;

        public TestService(IRepository<Article> repository)
        {
            _repository = repository;
        }

        public async Task DoAsync()
        {
            var artile = new Article
            {
                FullText = $"qweqweqwe{new Random().Next()}",
                Name = $"qqqqqq{new Random().Next()}"
            };
            await _repository.AddAsync(artile);
            await _repository.SaveChangesAsync();
        }
    }
}
